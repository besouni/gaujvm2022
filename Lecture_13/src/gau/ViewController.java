package gau;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.Random;

public class ViewController {
    @FXML
    private AnchorPane main;

    public void add(){
//        Platform.exit();
//        main.getChildren().clear();

        VBox vBox = new VBox();
        vBox.setLayoutY(60);
        vBox.setLayoutX(30);
        vBox.getChildren().clear();


        for(int i=0; i<5; i++) {
            HBox hBox = new HBox();
            hBox.setSpacing(10);
            vBox.getChildren().add(hBox);
            Random random = new Random();
            for(int j=0; j<4; j++) {
                String rand = String.valueOf(random.nextInt(-2, 2));
                Button b1 = new Button(rand);
                b1.setPrefWidth(30);
                hBox.getChildren().add(b1);
            }
        }
        vBox.setSpacing(10);
        main.getChildren().add(vBox);
    }
}
