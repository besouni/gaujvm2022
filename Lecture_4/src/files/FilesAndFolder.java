package files;

import java.io.*;

public class FilesAndFolder {
    public void toWrite(String t){
        try {
            FileWriter writer = new FileWriter("test.txt", true);
            writer.write(t);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void read(){
        try {
            BufferedReader rd = new BufferedReader(new FileReader("test.txt"));
//            System.out.println(rd.readLine());
//            System.out.println(rd.readLine());
            String s;
            while ((s=rd.readLine())!=null){
                System.out.println(s);
            }
            rd.close();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
